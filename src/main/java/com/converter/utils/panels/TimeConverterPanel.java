package main.java.com.converter.utils.panels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TimeConverterPanel extends JPanel {
    JLabel toFromMessage = new JLabel("Choose to or from sec: ");
    JLabel measureMessage = new JLabel("Choose measure: ");
    JLabel inputMessage = new JLabel("Input value: ");
    JComboBox<String> toFromChoice = new JComboBox<>();
    JComboBox<String> measureChoice = new JComboBox<>();
    JTextField inputField = new JTextField();
    JTextField outputField = new JTextField();
    JButton convertButton = new JButton("Convert");


    public TimeConverterPanel(){
        this.setLayout(new GridLayout(4, 2, 5, 5));
        toFromChoice.addItem("from sec");
        toFromChoice.addItem("to sec");

        measureChoice.addItem("Min");
        measureChoice.addItem("Hour");
        measureChoice.addItem("Day");
        measureChoice.addItem("Week");
        measureChoice.addItem("Month");
        measureChoice.addItem("Astronomical Year");

        convertButton.addActionListener(new ConvertButtonListener());

        this.add(toFromMessage);
        this.add(toFromChoice);
        this.add(measureMessage);
        this.add(measureChoice);
        this.add(inputMessage);
        this.add(inputField);
        this.add(convertButton);
        this.add(outputField);
    }

    public class ConvertButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            String fromToValue = toFromChoice.getSelectedItem().toString();
            String measureValue = measureChoice.getSelectedItem().toString();
            String inputValue = inputField.getText();
            inputValue = inputValue.trim();
            try{
                double input = Double.parseDouble(inputValue);

                String resultStr = "";
                double result = 0.0;
                if (fromToValue.equals("from sec")) {
                    switch (measureValue) {
                        case "Min":
                            result = input / 60;
                            break;
                        case "Hour":
                            result = input / 3600;
                            break;
                        case "Day":
                            result = (input / 3600) / 24;
                            break;
                        case "Week":
                            result = ((input / 3600) / 24) / 7;
                            break;
                        case "Month":
                            result = (((input / 3600) / 24) / 7) / 30;
                            break;
                        case "Astronomical Year":
                            result = ((((input / 3600) / 24) / 7) / 30) / 365;
                            break;
                    }
                }
                else {
                    switch (measureValue) {
                        case "Min":
                            result = input * 60;
                            break;
                        case "Hour":
                            result = input * 3600;
                            break;
                        case "Day":
                            result = (input * 3600) * 24;
                            break;
                        case "Week":
                            result = ((input * 3600) * 24) * 7;
                            break;
                        case "Month":
                            result = (((input * 3600) * 24) * 7) * 30;
                            break;
                        case "Astronomical Year":
                            result = ((((input * 3600) * 24) * 7) * 30) * 365;
                            break;
                    }
                }

                resultStr = resultStr.concat(String.valueOf(result));

                outputField.setText(resultStr);
            }
            catch (NumberFormatException exception){
                outputField.setText("Incorrect input");
            }
        }
    }
}
