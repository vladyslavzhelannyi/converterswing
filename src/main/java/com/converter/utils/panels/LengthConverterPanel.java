package main.java.com.converter.utils.panels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LengthConverterPanel extends JPanel {
    JLabel toFromMessage = new JLabel("Choose to or from m: ");
    JLabel measureMessage = new JLabel("Choose measure: ");
    JLabel inputMessage = new JLabel("Input value: ");
    JComboBox<String> toFromChoice = new JComboBox<>();
    JComboBox<String> measureChoice = new JComboBox<>();
    JTextField inputField = new JTextField();
    JTextField outputField = new JTextField();
    JButton convertButton = new JButton("Convert");


    public LengthConverterPanel(){
        this.setLayout(new GridLayout(4, 2, 5, 5));
        toFromChoice.addItem("from m");
        toFromChoice.addItem("to m");

        measureChoice.addItem("km");
        measureChoice.addItem("mile");
        measureChoice.addItem("nautical mile");
        measureChoice.addItem("cable");
        measureChoice.addItem("league");
        measureChoice.addItem("foot");
        measureChoice.addItem("yard");

        convertButton.addActionListener(new LengthConverterPanel.ConvertButtonListener());

        this.add(toFromMessage);
        this.add(toFromChoice);
        this.add(measureMessage);
        this.add(measureChoice);
        this.add(inputMessage);
        this.add(inputField);
        this.add(convertButton);
        this.add(outputField);
    }

    public class ConvertButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String fromToValue = toFromChoice.getSelectedItem().toString();
            String measureValue = measureChoice.getSelectedItem().toString();
            String inputValue = inputField.getText();
            inputValue = inputValue.trim();
            try{
                double input = Double.parseDouble(inputValue);

                String resultStr = "";
                double result = 0.0;
                if (fromToValue.equals("from m")) {
                    switch (measureValue) {
                        case "km":
                            result = input * 0.001;
                            break;
                        case "mile":
                            result = input * 0.000621371;
                            break;
                        case "nautical mile":
                            result = input * 0.000539957;
                            break;
                        case "cable":
                            result = input / 219.456;
                            break;
                        case "league":
                            result = input / 4179.5;
                            break;
                        case "foot":
                            result = input * 3.28084;
                            break;
                        case "yard":
                            result = input * 1.09361;
                            break;
                    }
                }
                else {
                    switch (measureValue) {
                        case "km":
                            result = input / 0.001;
                            break;
                        case "mile":
                            result = input / 0.000621371;
                            break;
                        case "nautical mile":
                            result = input / 0.000539957;
                            break;
                        case "cable":
                            result = input * 219.456;
                            break;
                        case "league":
                            result = input * 4179.5;
                            break;
                        case "foot":
                            result = input / 3.28084;
                            break;
                        case "yard":
                            result = input / 1.09361;
                            break;
                    }
                }

                resultStr = resultStr.concat(String.valueOf(result));

                outputField.setText(resultStr);
            }
            catch (NumberFormatException exception){
                outputField.setText("Incorrect input");
            }
        }
    }
}
