package main.java.com.converter.utils.panels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TemperatureConverterPanel extends JPanel {
    JLabel toFromMessage = new JLabel("Choose to or from C: ");
    JLabel measureMessage = new JLabel("Choose measure: ");
    JLabel inputMessage = new JLabel("Input value: ");
    JComboBox<String> toFromChoice = new JComboBox<>();
    JComboBox<String> measureChoice = new JComboBox<>();
    JTextField inputField = new JTextField();
    JTextField outputField = new JTextField();
    JButton convertButton = new JButton("Convert");


    public TemperatureConverterPanel(){
        this.setLayout(new GridLayout(4, 2, 5, 5));
        toFromChoice.addItem("from C");
        toFromChoice.addItem("to C");

        measureChoice.addItem("K");
        measureChoice.addItem("F");
        measureChoice.addItem("Re");
        measureChoice.addItem("Ro");
        measureChoice.addItem("Ra");
        measureChoice.addItem("N");
        measureChoice.addItem("Dv");

        convertButton.addActionListener(new TemperatureConverterPanel.ConvertButtonListener());

        this.add(toFromMessage);
        this.add(toFromChoice);
        this.add(measureMessage);
        this.add(measureChoice);
        this.add(inputMessage);
        this.add(inputField);
        this.add(convertButton);
        this.add(outputField);
    }

    public class ConvertButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String fromToValue = toFromChoice.getSelectedItem().toString();
            String measureValue = measureChoice.getSelectedItem().toString();
            String inputValue = inputField.getText();
            inputValue = inputValue.trim();
            try{
                double input = Double.parseDouble(inputValue);

                String resultStr = "";
                double result = 0.0;
                if (fromToValue.equals("from C")) {
                    switch (measureValue) {
                        case "K":
                            result = input + 237.15;
                            break;
                        case "F":
                            result = (input * 9 / 5) + 32;
                            break;
                        case "Re":
                            result = input * 0.8;
                            break;
                        case "Ro":
                            result = input * 0.525 + 7.5;
                            break;
                        case "Ra":
                            result = input * 1.8 + 491.67;
                            break;
                        case "N":
                            result = input * 0.33;
                            break;
                        case "Dv":
                            result = input * 1.5 - 100;
                            break;
                    }
                }
                else {
                    switch (measureValue) {
                        case "K":
                            result = input - 237.15;
                            break;
                        case "F":
                            result = (input - 32) / 9 * 5;
                            break;
                        case "Re":
                            result = input / 0.8;
                            break;
                        case "Ro":
                            result = (input - 7.5) / 0.525;
                            break;
                        case "Ra":
                            result = (input - 491.67) / 1.8;
                            break;
                        case "N":
                            result = input / 0.33;
                            break;
                        case "Dv":
                            result = (input + 100) / 1.5;
                            break;
                    }
                }

                resultStr = resultStr.concat(String.valueOf(result));

                outputField.setText(resultStr);
            }
            catch (NumberFormatException exception){
                outputField.setText("Incorrect input");
            }
        }
    }
}
