package main.java.com.converter.utils.panels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VolumeConverterPanel extends JPanel {
    JLabel toFromMessage = new JLabel("Choose to or from l: ");
    JLabel measureMessage = new JLabel("Choose measure: ");
    JLabel inputMessage = new JLabel("Input value: ");
    JComboBox<String> toFromChoice = new JComboBox<>();
    JComboBox<String> measureChoice = new JComboBox<>();
    JTextField inputField = new JTextField();
    JTextField outputField = new JTextField();
    JButton convertButton = new JButton("Convert");


    public VolumeConverterPanel(){
        this.setLayout(new GridLayout(4, 2, 5, 5));
        toFromChoice.addItem("from l");
        toFromChoice.addItem("to l");

        measureChoice.addItem("m^3");
        measureChoice.addItem("gallon");
        measureChoice.addItem("pint");
        measureChoice.addItem("quart");
        measureChoice.addItem("barrel");
        measureChoice.addItem("cubic foot");
        measureChoice.addItem("cubic inch");

        convertButton.addActionListener(new VolumeConverterPanel.ConvertButtonListener());

        this.add(toFromMessage);
        this.add(toFromChoice);
        this.add(measureMessage);
        this.add(measureChoice);
        this.add(inputMessage);
        this.add(inputField);
        this.add(convertButton);
        this.add(outputField);
    }

    public class ConvertButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String fromToValue = toFromChoice.getSelectedItem().toString();
            String measureValue = measureChoice.getSelectedItem().toString();
            String inputValue = inputField.getText();
            inputValue = inputValue.trim();
            try{
                double input = Double.parseDouble(inputValue);

                String resultStr = "";
                double result = 0.0;
                if (fromToValue.equals("from l")) {
                    switch (measureValue) {
                        case "m^3":
                            result = input * 0.001;
                            break;
                        case "gallon":
                            result = input * 0.264172;
                            break;
                        case "pint":
                            result = input * 1.75975;
                            break;
                        case "quart":
                            result = input * 0.879877;
                            break;
                        case "barrel":
                            result = input / 158.988;
                            break;
                        case "cubic foot":
                            result = input * 0.0353147;
                            break;
                        case "cubic inch":
                            result = input * 61.0237;
                            break;
                    }
                }
                else {
                    switch (measureValue) {
                        case "m^3":
                            result = input / 0.001;
                            break;
                        case "gallon":
                            result = input / 0.264172;
                            break;
                        case "pint":
                            result = input / 1.75975;
                            break;
                        case "quart":
                            result = input / 0.879877;
                            break;
                        case "barrel":
                            result = input * 158.988;
                            break;
                        case "cubic foot":
                            result = input / 0.0353147;
                            break;
                        case "cubic inch":
                            result = input / 61.0237;
                            break;
                    }
                }

                resultStr = resultStr.concat(String.valueOf(result));

                outputField.setText(resultStr);
            }
            catch (NumberFormatException exception){
                outputField.setText("Incorrect input");
            }
        }
    }
}
