package main.java.com.converter.utils.panels;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WeightConverterPanel extends JPanel {
    JLabel toFromMessage = new JLabel("Choose to or from kg: ");
    JLabel measureMessage = new JLabel("Choose measure: ");
    JLabel inputMessage = new JLabel("Input value: ");
    JComboBox<String> toFromChoice = new JComboBox<>();
    JComboBox<String> measureChoice = new JComboBox<>();
    JTextField inputField = new JTextField();
    JTextField outputField = new JTextField();
    JButton convertButton = new JButton("Convert");


    public WeightConverterPanel(){
        this.setLayout(new GridLayout(4, 2, 5, 5));
        toFromChoice.addItem("from kg");
        toFromChoice.addItem("to kg");

        measureChoice.addItem("g");
        measureChoice.addItem("carat");
        measureChoice.addItem("eng pound");
        measureChoice.addItem("pound");
        measureChoice.addItem("stone");
        measureChoice.addItem("rus pound");

        convertButton.addActionListener(new WeightConverterPanel.ConvertButtonListener());

        this.add(toFromMessage);
        this.add(toFromChoice);
        this.add(measureMessage);
        this.add(measureChoice);
        this.add(inputMessage);
        this.add(inputField);
        this.add(convertButton);
        this.add(outputField);
    }

    public class ConvertButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String fromToValue = toFromChoice.getSelectedItem().toString();
            String measureValue = measureChoice.getSelectedItem().toString();
            String inputValue = inputField.getText();
            inputValue = inputValue.trim();
            try{
                double input = Double.parseDouble(inputValue);

                String resultStr = "";
                double result = 0.0;
                if (fromToValue.equals("from kg")) {
                    switch (measureValue) {
                        case "g":
                            result = input * 1000;
                            break;
                        case "carat":
                            result = input * 5000;
                            break;
                        case "eng pound":
                            result = input / 0.45359237;
                            break;
                        case "pound":
                            result = input * 2.20462;
                            break;
                        case "stone":
                            result = input * 0.157473;
                            break;
                        case "rus pound":
                            result = input / 0.40951241;
                            break;
                    }
                }
                else {
                    switch (measureValue) {
                        case "g":
                            result = input / 1000;
                            break;
                        case "carat":
                            result = input / 5000;
                            break;
                        case "eng pound":
                            result = input * 0.45359237;
                            break;
                        case "pound":
                            result = input / 2.20462;
                            break;
                        case "stone":
                            result = input / 0.157473;
                            break;
                        case "rus pound":
                            result = input * 0.40951241;
                            break;
                    }
                }

                resultStr = resultStr.concat(String.valueOf(result));

                outputField.setText(resultStr);
            }
            catch (NumberFormatException exception){
                outputField.setText("Incorrect input");
            }
        }
    }
}
