package main.java.com.converter.services;

import main.java.com.converter.utils.panels.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ConverterService extends JFrame {

    JPanel buttonPanel = new JPanel();
    JPanel panelsPanel = new JPanel();
    JPanel converterPanel = new TimeConverterPanel();

    JButton timeConverterButton = new JButton("Time converter");
    JButton volumeConverterButton = new JButton("Volume converter");
    JButton temperatureConverterButton = new JButton("Temperature converter");
    JButton lengthConverterButton = new JButton("Length converter");
    JButton weightConverterButton = new JButton("Weight converter");

    private int windowWidth = 800;
    private int windowHeight = 600;

    public ConverterService(){
        run();
    }

    public void run(){
        this.setName("Converter");
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension dimension = toolkit.getScreenSize();
        this.setBounds(dimension.width / 2 - windowWidth / 2, dimension.height / 2 - windowHeight / 2, windowWidth,
                windowHeight);

        Container container = this.getContentPane();
        container.setLayout(new GridLayout(2, 1, 5, 5));

        buttonPanel.setLayout(new FlowLayout());

        timeConverterButton.addActionListener(new PanelTimeSwitchListener());
        weightConverterButton.addActionListener(new PanelWeightSwitchListener());
        volumeConverterButton.addActionListener(new PanelVolumeSwitchListener());
        lengthConverterButton.addActionListener(new PanelLengthSwitchListener());
        temperatureConverterButton.addActionListener(new PanelTemperatureSwitchListener());

        buttonPanel.add(timeConverterButton);
        buttonPanel.add(weightConverterButton);
        buttonPanel.add(volumeConverterButton);
        buttonPanel.add(lengthConverterButton);
        buttonPanel.add(temperatureConverterButton);

        panelsPanel.setLayout(new GridLayout(1, 1, 5, 5));
        panelsPanel.add(converterPanel);

        container.add(buttonPanel);
        container.add(panelsPanel);
    }

    private void switchPanel(JPanel jPanel){
        panelsPanel.removeAll();
        panelsPanel.add(jPanel);
        panelsPanel.repaint();
        panelsPanel.revalidate();
    }

    public class PanelTimeSwitchListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent event) {
            switchPanel(new TimeConverterPanel());
        }
    }

    public class PanelWeightSwitchListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent event) {
            switchPanel(new WeightConverterPanel());
        }
    }

    public class PanelVolumeSwitchListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent event) {
            switchPanel(new VolumeConverterPanel());
        }
    }

    public class PanelLengthSwitchListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent event) {
            switchPanel(new LengthConverterPanel());
        }
    }

    public class PanelTemperatureSwitchListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent event) {
            switchPanel(new TemperatureConverterPanel());
        }
    }
}
